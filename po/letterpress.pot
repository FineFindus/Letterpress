# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the letterpress package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: letterpress\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-06-06 18:10+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: Translate as "Letterpress"
#: data/io.gitlab.gregorni.ASCIIImages.desktop.in:4
#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:7 src/main.py:137
#: src/gtk/window.blp:5
msgid "Letterpress"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.desktop.in:5
msgid "Convert images into ASCII characters"
msgstr ""

#. Translators: These are search terms for the application;
#. the translation should contain the original list AND the translated strings.
#. The list must end with a semicolon.
#: data/io.gitlab.gregorni.ASCIIImages.desktop.in:14
msgid "image;ascii;convert;conversion;text;"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:8 src/main.py:139
msgid "Letterpress Contributors"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:9
msgid "Create beautiful ASCII art"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:12
msgid ""
"Letterpress converts your images into a picture made up of ASCII characters. "
"You can save the output to a file, copy it, and even change its resolution! "
"High-res output can still be viewed comfortably by lowering the zoom factor."
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:28
msgid "image"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:29
msgid "ascii"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:30
msgid "convert"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:31
msgid "conversion"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:32
msgid "text"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:49
msgid "Overview"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:53
msgid "Copy output"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:57
msgid "Save output"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:61
msgid "Adjust output width"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:84
msgid "Rebrand to Letterpress"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:85
msgid "Replaced icon on welcome screen with illustration"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:86
msgid "Changed warning dialog to Adw.MessageDialog"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:87
msgid "Improved adaptiveness of \"Output Width\" subtitle"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:88
msgid "Swapped buttons for copying and saving output"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:89
msgid "Added a string to translations"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:90
msgid "Added Turkish translation"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:91
msgid "Added Czech translation"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:98
msgid "Added French translation"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:99
msgid "Added Russian translation"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:100
msgid "Added Occitan translation"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:101
msgid "Added Italian translation"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:102
msgid "Fixed \"Open with…\" functionality"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:103
msgid "The app now remembers window size and state"
msgstr ""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:109
msgid "First Release! 🎉"
msgstr ""

#. Translators: Translate this string as your translator credits.
#. Name only:    gregorni
#. Name + URL:   gregorni https://gitlab.com/gregorni/
#. Name + Email: gregorni <gregorniehl@web.de>
#: src/main.py:147
msgid "translator-credits"
msgstr ""

#: src/main.py:148
msgid "Copyright © 2023 Letterpress Contributors"
msgstr ""

#: src/main.py:156
msgid "Code and Design borrowed from"
msgstr ""

#: src/window.py:95
msgid "Width of the ASCII image in characters"
msgstr ""

#. Translators: Do not translate "{basename}"
#: src/window.py:120
#, python-brace-format
msgid "\"{basename}\" is not of a supported image type."
msgstr ""

#: src/window.py:190
msgid "The output is too large to be copied."
msgstr ""

#: src/window.py:192
msgid "Please save it to a file instead or decrease the output width."
msgstr ""

#: src/window.py:195
msgid "_OK"
msgstr ""

#: src/window.py:199
msgid "Output copied to clipboard"
msgstr ""

#. Translators: Do not translate "{display_name}"
#: src/window.py:230
#, python-brace-format
msgid "Unable to save \"{display_name}\""
msgstr ""

#. Translators: Do not translate "{display_name}"
#: src/window.py:237
#, python-brace-format
msgid "\"{display_name}\" saved"
msgstr ""

#: src/window.py:239
msgid "Open"
msgstr ""

#: src/file_chooser.py:38 src/file_chooser.py:61
msgid "Select a file"
msgstr ""

#: src/file_chooser.py:45
msgid "Supported image files"
msgstr ""

#: src/gtk/window.blp:17
msgid "Main Menu"
msgstr ""

#: src/gtk/window.blp:46
msgid "Create ASCII Art"
msgstr ""

#: src/gtk/window.blp:55
msgid "Open or drag and drop an image to generate an ASCII art version of it"
msgstr ""

#: src/gtk/window.blp:65
msgid "Open File…"
msgstr ""

#: src/gtk/window.blp:82
msgid "Drop Here to Open"
msgstr ""

#: src/gtk/window.blp:143
msgid "Output Width"
msgstr ""

#: src/gtk/window.blp:161
msgid "Copy to clipboard"
msgstr ""

#: src/gtk/window.blp:168
msgid "Save to file"
msgstr ""

#: src/gtk/window.blp:191
msgid "_Open File…"
msgstr ""

#: src/gtk/window.blp:198
msgid "_Keyboard Shortcuts"
msgstr ""

#: src/gtk/window.blp:203
msgid "_About Letterpress"
msgstr ""

#: src/gtk/zoom-box.blp:12
msgid "Zoom out"
msgstr ""

#: src/gtk/zoom-box.blp:28
msgid "Reset zoom"
msgstr ""

#: src/gtk/zoom-box.blp:40
msgid "Zoom in"
msgstr ""

#: src/gtk/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr ""

#: src/gtk/help-overlay.blp:14
msgctxt "shortcut window"
msgid "Open File"
msgstr ""

#: src/gtk/help-overlay.blp:19
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr ""

#: src/gtk/help-overlay.blp:24
msgctxt "shortcut window"
msgid "Quit"
msgstr ""

#: src/gtk/help-overlay.blp:30
msgctxt "shortcut window"
msgid "Text View"
msgstr ""

#: src/gtk/help-overlay.blp:33
msgctxt "shortcut window"
msgid "Zoom in"
msgstr ""

#: src/gtk/help-overlay.blp:38
msgctxt "shortcut window"
msgid "Zoom out"
msgstr ""
